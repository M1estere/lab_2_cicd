using System;

public interface IStoreItem
{
    double Price { get; set; }

    void DiscountPrice(int percent);
}