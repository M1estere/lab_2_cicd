using System;

internal class Audio : Disk
{
    private string _artist;
    private string _recordingStudio;

    private int _songsNumber;

    internal override int DiskSize { get => (_songsNumber * 8); }

    internal Audio(string name, string genre, string artist, string recordingStudio, int songsNumber) : base(name, genre)
    {
        _artist = artist;
        _recordingStudio = recordingStudio;
        _songsNumber = songsNumber;
    }

    internal override void Burn(params string[] values)
    {
        if (values.Length < 5)
        {
            Console.WriteLine("!Attention: cannot burn this Audio Disk, too less parameters given!");
            return;
        }
        Console.WriteLine($"!{Name} was burnt with {values[0]} by {values[2]}");

        Name = values[0];
        Genre = values[1];

        _artist = values[2];
        _recordingStudio = values[3];

        _songsNumber = Convert.ToInt32(values[4]);

        BurnCount++;
    }

    public override string ToString()
    {
        return $"Audio Disk:\n\tName: {Name},\n\tGenre: {Genre},\n\tArist: {_artist},\n\t" +
                $"Recording Studio: {_recordingStudio},\n\tSongs amount: {_songsNumber},\n\tBurns: {BurnCount}";
    }
}