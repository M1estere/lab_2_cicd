using System;

internal class Disk : IStoreItem
{
    public string Name { get; protected set; }
    public string Genre { get; protected set; }
    protected int BurnCount { get; set; }

    internal virtual int DiskSize { get; }
    public double Price { get; set; }

    internal Disk(string name, string genre)
    {
        Name = name;
        Genre = genre;
    }

    internal virtual void Burn(params string[] values) { }

    public void DiscountPrice(int percent)
    {
        Price = ((100 - percent) * Price) / 100;
    }
}