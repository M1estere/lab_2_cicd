using System;
using System.Collections.Generic;

internal class Store
{
    internal string StoreName { get; }
    internal string Address { get; }

    internal List<Audio> Audios { get; set; } = new List<Audio>();
    internal List<DVD> Films { get; set; } = new List<DVD>();

    public Store(string storeName, string address)
    {
        StoreName = storeName;
        Address = address;
    }

    /* Такой вариант казался круче, чтобы просто писать: store += new Audio();
    public static Store operator +(Store store, Audio newAudio)
    {
        store.Audios.Add(newAudio);

        return store;
    }*/

    public static List<Audio> operator +(Store store, Audio newAudio)
    {
        store.Audios.Add(newAudio);

        return store.Audios;
    }

    public static List<Audio> operator -(Store store, Audio audioToRemove)
    {
        if (store.Audios.Contains(audioToRemove) == false) return store.Audios;

        store.Audios.Remove(audioToRemove);

        return store.Audios;
    }

    public static List<Audio> operator +(Store store, Audio[] audiosToAdd)
    {
        foreach (Audio audio in audiosToAdd)
        {
            store.Audios.Add(audio);
        }

        return store.Audios;
    }

    public static List<Audio> operator -(Store store, Audio[] audiosToAdd)
    {
        foreach (Audio audio in audiosToAdd)
        {
            if (store.Audios.Contains(audio) == false)
                continue;

            store.Audios.Remove(audio);
        }

        return store.Audios;
    }

    public static List<DVD> operator +(Store store, DVD newDvd)
    {
        store.Films.Add(newDvd);

        return store.Films;
    }

    public static List<DVD> operator -(Store store, DVD dvdToRemove)
    {
        if (store.Films.Contains(dvdToRemove) == false) return store.Films;

        store.Films.Remove(dvdToRemove);

        return store.Films;
    }

    public static List<DVD> operator +(Store store, DVD[] filmsToAdd)
    {
        foreach (DVD dvd in filmsToAdd)
        {
            store.Films.Add(dvd);
        }

        return store.Films;
    }

    public static List<DVD> operator -(Store store, DVD[] filmsToRemove)
    {
        foreach (DVD film in filmsToRemove)
        {
            if (store.Films.Contains(film) == false)
                continue;

            store.Films.Remove(film);
        }

        return store.Films;
    }

    public override string ToString()
    {
        string resultString = "";

        resultString += $"Store: {StoreName} on {Address}\n";
        resultString += "Audios:\n";

        int counter = 0;
        foreach (Audio audio in Audios)
        {
            counter++;
            resultString += $"\t{counter}. {audio.Name}, {audio.Genre}";
            resultString += "\n";
        }

        counter = 0;
        resultString += "Films:\n";
        foreach (DVD dvd in Films)
        {
            counter++;
            resultString += $"\t{counter}. {dvd.Name}, {dvd.Genre}";
            resultString += "\n";
        }

        return resultString;
    }
}