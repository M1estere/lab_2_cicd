using System;

internal static class Program
{
    private static void Main(string[] args)
    {
        Store store = new Store("Disks Store", "Baker Street, 221B");

        Audio firstAudio = new Audio("80's best hits", "Action", "Greatest Artists", "80's superheroes", 15);
        Audio secondAudio = new Audio("Whaaaaaat the album", "Pop", "Backstreet Boys", "2000's men", 35);
        Audio thirdAudio = new Audio("Best Hits", "Pop", "Maroon 5", "BBC", 15);

        DVD firstFilm = new DVD("Godzilla", "Comedy", "Who", "Same shit", 184);
        DVD secondFilm = new DVD("Spider-Man", "Drama", "IDK", "HZ", 123);
        DVD thirdFilm = new DVD("1+1", "Horror", "Don't remember", "Who knows", 147);

        store.Audios = store + new Audio[] { firstAudio, secondAudio, thirdAudio };

        store.Films = store + new DVD[] { firstFilm, secondFilm, thirdFilm };

        Console.WriteLine(store.ToString());
        Console.WriteLine("\n");

        store.Audios[1].Burn("Beat My Soul", "Fighting", "Mike Tyson", "HDStudio", "83");
        store.Films[0].Burn("Mary Poppins", "Fighting", "No Idea", "No Idea x2", "153");
        Console.WriteLine("\n");

        store.Films = store - secondFilm;
        store.Audios = store - thirdAudio;

        Console.WriteLine($"All disks info of store {store.StoreName} on {store.Address}");
        Console.WriteLine("Audio Disks:\n\tTitle -> Size");

        int counter = 1;
        foreach (Audio storeAudio in store.Audios)
        {
            Console.WriteLine($"\t{counter}. {storeAudio.Name} -> {storeAudio.DiskSize}");
            counter++;
        }

        Console.WriteLine("DVD Disks:\n\tTitle -> Size");

        counter = 1;
        foreach (DVD storeDvd in store.Films)
        {
            Console.WriteLine($"\t{counter}. {storeDvd.Name} -> {storeDvd.DiskSize}");
            counter++;
        }
    }
}