using System;

internal class DVD : Disk
{
    private string _producer;
    private string _filmCompany;

    private int _minutesCount;

    internal DVD(string name, string genre, string producer, string filmCompany, int minutesCount) : base(name, genre)
    {
        _producer = producer;
        _filmCompany = filmCompany;
        _minutesCount = minutesCount;
    }

    internal override int DiskSize { get => ((_minutesCount / 64) * 2); }

    internal override void Burn(params string[] values)
    {
        if (values.Length < 5)
        {
            Console.WriteLine("!Attention: cannot burn this DVD Disk, too less parameters given!");
            return;
        }
        Console.WriteLine($"!{Name} was burnt with {values[0]} by {values[2]}");

        Name = values[0];
        Genre = values[1];
        _producer = values[2];
        _filmCompany = values[3];

        _minutesCount = Convert.ToInt32(values[4]);

        BurnCount++;
    }

    public override string ToString()
    {
        return $"DVD Disk:\n\tName: {Name},\n\tGenre: {Genre},\n\tProducer: {_producer},\n\t" +
                $"Film Company: {_filmCompany},\n\tLength: {_minutesCount} min,\n\tBurns: {BurnCount}";
    }
}